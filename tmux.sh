#!/bin/sh
# set Session Name
SESSION="Genpro"
SESSIONEXISTS=$(tmux ls|grep $SESSION)

# only create tmux session if it doesn't already exists
if [ "$SESSIONSEXISTS" = "" ]
then
    # start New Session with name "Genpro"

    tmux new-session -d -s $SESSION

    # Name first Pane and start bash
    tmux rename-window -t 0 'docker'
    tmux send-keys -t 'docker' 'sudo service docker start' C-m
    tmux new-window -t $SESSION:1 -n 'git-push'
    tmux send-keys -t 'git-push' 'cd disease-context-center/backend/server ;pipenv shell ' C-m 
    tmux new-window -t $SESSION:2 -n 'docker-compose-up'
    tmux send-keys -t 'docker-compose-up' 'cd disease-context-center;code .' C-m
    tmux new-window -t $SESSION:3 -n 'python3-vim'
    tmux send-keys -t 'python3-vim' 'python3' C-m 
    tmux split-window -h 'vim'

fi 

# Attach Session, on the Main window

tmux attach-session -t $SESSION:0
